# Projet Infra/Sécu - Ynov Informatique 2022/2023

## Groupe
> Thomas CURMI - Infra
> Benjamin Gellineau - Sécu
> Hugo JOYET - Sécu

# 1. Problématique du projet

- Notre Projet a pour problématique : Réaliser une infrastructure sécurisée avec peu de ressources.

## 2. Choix de notre solution

### 2.1 Infrastructure 

- Afin de répondre à la problématique, nous avons décidé de réaliser une architecture virtualisée se basant sur KVM. De plus, les machines virtuelles seront utilisées dans un cluster K3S afin d'héberger une stack wordpress et autres.

### 2.2 Sécurité

- Afin d'assurer un haut niveau de sécurité de notre solution, nous allons mettre en place les restrictions nécessaire a tous les services et protocoles que nous utilisons ainsi que la configuration d'un firewall. De plus, nous allons mettre en place une grande partie des mesures de durcissement proposées par les guides de CIS Benchmark.

# Sommaire

- [Serveur physique](#serveur-physique)
    - [Firewall](#firewall)
    - [Configuration des utilisateurs](#configuration-des-utilisateurs)
    - [Accès SSH](#accès-ssh)
    - [Fail2Ban](#configuration-fail2ban)
    - [Installation QEMU/KVM](#installation-qemukvm)
        - [Creation des VMs](#création-des-vms)
        - [Configuration des IPs](#configuration-des-ips-statiques-de-vm)
- [Configuration des VMs](#Configuration-des-VMs)
    - [Installation de K3S](#Installation-de-K3S)
    - [Déploiement d'un Wordpress à la main](#Déploiement-d'un-wordpress-à-la-main)
    - [Déploiement de la stack Prometheus via Helm](#Déploiement-de-la-stack-Prometheus-via-Helm)

- [Registre d'images signées](#registre-dimages-signées)
    - [Trivy](#trivy)
    - [Notary](#notary)

- [CIS Benchmark](#cis-benchmark)
    - [Partitionnement](#partitionnement)
    - [SSH](#ssh)


# Serveur physique

Le serveur physique est un ancien PC reconditionné, qui est également utilisé pour d'autres tâches.
Pour le choix de l'OS, puisque le serveur est utilisé dans un autre cadre que le cours, Debian11 était déjà installé.
Rocky Linux pour sa robustesse en plus d'être un OS avec lequel nous nous sommes familiarisé au cours des précédentes années aurait cependant été plus pertinent.

- Caractéristiques physique
    - Processeur i7 6700 (4 coeurs / 8 threads)
    - 16 Go RAM 2133 Mhz
    - 480 Go stockage SSD
- Installation par défault de Debian11 minimal (sans IDE)
    - Configuration de eth0 en IP statique

Représentation de l'infrastructure finale :

![name](./pics/final.png)

## Firewall

Configuration du firewall sur le serveur physique en utilisant iptables.
On bloque tout sauf le néccessaire.
```shell
# SSH
iptables -A INPUT -p tcp -m tcp --dport 19732 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --sport 19732 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Allow all local traffic
iptables -A INPUT -s 192.168.1.0/24 -j ACCEPT
iptables -A OUTPUT -d 192.168.1.0/24 -j ACCEPT

# Allow ping out and allow pong response
iptables -A INPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT
iptables -A OUTPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT

# Allow internet traffic
iptables -A INPUT -p tcp -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Allow loopback
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Drop the rest
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
```

## Configuration des utilisateurs

Le compte de Thomas étant déjà paramétré à l'installation de la machine, nous rajoutons les comptes pour Hugo et Benjamin afin qu'il puisse par la suite se connecter en SSH avec leurs utilisateurs respectifs
Ils n'auront pas accès aux droits root.

```shell
# Création de l'utilisateur
# -m > afin de créer son répertoire personnel
# -s > pour lui assigner un shell afin qu'il puisse s'y connecter
useradd -m -s /bin/bash hugo

# Ajout d'un mot de passe à l'utilisateur sinon il ne pourra pas se connecter
# je mets un mot de passe générique pour la première fois il devra le changer par la suite
# --expire > permet de forcer le changement de mot de passe à la prochaine connexion
passwd --expire hugo

# Configuration pour SSH
mkdir /home/hugo/.ssh
touch /home/hugo/.ssh/authorized_keys
# On dépose ensuite sa clé publique dans le ficher authorized_keys

# On termine par mettre les bons droits sur les fichiers
# -R > permet d'appliquer la commande à tous les fichiers et sous-dossiers du dossier ciblé
chown -R hugo:hugo /home/hugo/.ssh
chmod 700 /home/hugo/.ssh && chmod 600 /home/hugo/.ssh/authorized_keys
```
Répéter la tache pour chaque utilisateurs

## Accès SSH

La configuration du serveur SSHD se trouve `/etc/ssh/sshd_config`
```sh
# On change le port
Port 19732

# On autorise les connexions par clé SSH
PubkeyAuthentication yes

# On refuse les connexions par mot de passe
PasswordAuthentication no

# Pour le reste on laisse les valeurs par défault
```

On termine par redémmarer le service
```sh
systemctl restart sshd
```

## Configuration fail2ban

On installe, démarre et active le service au démarrage
```sh
apt install fail2ban
systemctl enable --now fail2ban.service
```

Configuration du fail2ban
```sh
# On créer notre jail pour ssh à l'emplacement suivant
# /etc/fail2ban/jail.d/custom.conf
# on y ajoute les paramètres suivant

[sshd]
enabled = true
```
On configure ensuite le comportement de fail2ban en remplaçant celui par défaut
```sh
# jail.conf est le fichier par défault
# jail.local prend la priorité sur jail.conf
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

```sh
# On confgure ensuite notre jail.local
# remplacement du port d'écoute du serveur sshd
[sshd]
port = 19732

# On ignore les IP local afin d'éviter de se bloquer :)
ignoreip = 127.0.0.1/8 192.168.1.0/24

# l'intervalle de détection
findtime = 15m

# le nombre de détection
maxretry = 3

bantime = 999d
```
Ainsi fail2ban est configuré pour bloquer les IPs pendant 999 jours qui se connectent de manière infructeuse à 3 reprises sur notre port ssh sur un intervalle de 15 minutes.

## Installation QEMU/KVM

On commence par installer les paquets nécessairent

QEMU sera notre hyperviseur
libvirt nous permettra de controller notre hyperviseur en ligne de commande
```sh
apt install --no-install-recommends qemu-system libvirt-clients libvirt-daemon-system
```

On ajoute ensuite nos utilisateurs au groupe libvirt pour pouvoir manager nos VM par la suite
```sh
adduser thomas libvirt
adduser hugo libvirt
adduser benjamin libvirt
```

### Création des VMs

On commence par télécharger une image de Rocky Linux 9 au format `.qcow2` sur le site officiel.
Il ne s'agit pas d'un ISO mais directement d'une machine déjà installé sous la forme d'un disque.
Cette image possède par ailleurs cloud-init d'installer, que nous allons utilisé pour provisionner nos VMs et gagner un peu de temps sur leurs configurations.

Configuration de cloud-init user-data
- Création de notre utilisateur avec des droits root ainsi que dépôt des clés SSH pour se connecter a la VM par la suite
```yml
#cloud-config
users:
  - name: infra
    gecos: Super adminsys
    primary_group: projetinfra
    groups: wheel
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$yKkxovKMybNIR.wb$nif3D2xMJM4tumGfjavHrV4uV8jMylHGEgVa.jtQGHanB6fmDJDUPOlpF0U/2aFsFvCmcKE/Bs9QZAgRDgk9..
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG+9EF3AloQfE4xidhkwRQ1ntcCzH64c+hGLlbwRYdza benji@thefast
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIp9Om89/omxN4E4Y+Dyqea1K2oRc5LDbfwgZ6L1z0No thomas@aschu
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4dlptHJrbLIJkw54OD0nRrWagswAWlH61ga0Hcvnbu hugoj@vivobook
```
[user-data.yaml](config/cloud-init/user-data.yaml)

Configuration de cloud-init meta-data
- Configuration du hostname de la machine
```yml
local-hostname: m1.projet.infra
```
[meta-data.yaml](config/cloud-init/meta-data.yaml)

Une fois ses fichiers créés, il n'y a plus qu'a générer un ISO avec pour que cloud-init l'utilise et configure la VM lors de son premier démarrage
- A noter que le `volid` est important sinon la configuration ne sera pas lu
```sh
genisoimage -output cloud-init.iso -volid cidata -joliet -r meta-data user-data
```

Il ne nous reste plus qu'a créer notre première machine
```sh
# -n > sera le nom de la machine
# --ram et --vcpus > Valeurs défini pour nous, après avoir fait plusieurs tests, afin que les VM fonctionnent au mieux pour notre besoin par rapport aux limites physiques du serveur
# --disk > le premier sert à monter le disque de la machine, donc notre image .qcow2 téléchargé précédement, elle à cependant était déplacée et renommée
# --disk > le deuxième sert à monter notre ISO de configuration de cloud-init
# --network > sert à ajouter une carte réseau à la machine, virbr0 est un bridge qui donnera accès à internet à nos VM et leurs permettra de communiquer sur le même réseau virtuel
# --hvm > argument TRES IMPORTANT qui permet totalement virtualiser la machine virtuelle, cela utlise le module KVM et permet de gagner énormement de performance
# --import > permet de spécifier que la VM existe déjà puisque nous importons son disque depuis un qcow2
virt-install -n infra-master1 --os-type=Linux --os-variant=linux2020 --ram=2048 --vcpus=2 --graphics none --disk /home/aschu/VM-infra/master1/m1.qcow2 --disk /home/aschu/VM-infra/master1/cloud-init.iso --network bridge:virbr0 --hvm --import
```

On peut ensuite manipuler notre VM depuis `virsh`
```sh
# Pour rentrer dans le mode interactif de libvirt
virsh

# Afficher toutes les machinnes (appelés domaines)
# --all > si non spécifié, n'affiche que les VM qui sont allumés 
list --all

# Démarrer notre VM
start infra-master1

# Eteindre la VM
shutdown infra-master1

# Se connecter à la VM depuis virsh
console infra-master1
```

Répéter l'action pour configurer nos 7 VMs

### Configuration des IPs statiques de VM

Une fois toutes nos VM créées, nous allons les addressé statiquement puisque le réseau virtuel utilise son propore DHCP.

On commencer par obtenir la liste de nos VM ainsi que leurs addresses MAC (sur l'exemple elle sont déjà correctement adressés)
```sh
# Dans virsh
# defautl est le nom de virbr0
$ net-dhcp-leases default
 Expiry Time           MAC address         Protocol   IP address           Hostname   Client ID or DUID
------------------------------------------------------------------------------------------------------------
 2023-05-21 18:15:13   52:54:00:21:2a:5b   ipv4       192.168.122.11/24    w1-1       01:52:54:00:21:2a:5b
 2023-05-21 18:15:38   52:54:00:61:c4:85   ipv4       192.168.122.100/24   h1         01:52:54:00:61:c4:85
 2023-05-21 18:14:50   52:54:00:6a:8c:bf   ipv4       192.168.122.30/24    m3         01:52:54:00:6a:8c:bf
 2023-05-21 18:15:30   52:54:00:81:de:43   ipv4       192.168.122.31/24    w3-1       01:52:54:00:81:de:43
 2023-05-21 18:15:23   52:54:00:89:32:c6   ipv4       192.168.122.21/24    w2-1       01:52:54:00:89:32:c6
 2023-05-21 18:14:47   52:54:00:92:bf:ef   ipv4       192.168.122.20/24    m2         01:52:54:00:92:bf:ef
 2023-05-21 18:14:42   52:54:00:bb:c0:52   ipv4       192.168.122.10/24    m1         01:52:54:00:bb:c0:52

```

Puis on ajoute une ligne `host` à la configuration réseau de virbr0 pour faire une correspondance MAC/IP pour chaque VM
```sh
$ net-edit default
```
```xml
<network>
    <name>default</name>
    <uuid>6aaa702a-8c48-4b56-958f-69bcd06d80f6</uuid>
    <forward mode='nat'/>
    <bridge name='virbr0' stp='on' delay='0'/>
    <mac address='52:54:00:82:e9:2a'/>
    <ip address='192.168.122.1' netmask='255.255.255.0'/>
        <dhcp>
            <range start='192.168.122.100' end='192.168.122.250'/>
            <host mac='52:54:00:bb:c0:52' ip='192.168.122.10'/>
            <host mac='52:54:00:92:bf:ef' ip='192.168.122.20'/>
            <host mac='52:54:00:6a:8c:bf' ip='192.168.122.30'/>
            <host mac='52:54:00:21:2a:5b' ip='192.168.122.11'/>
            <host mac='52:54:00:89:32:c6' ip='192.168.122.21'/>
            <host mac='52:54:00:81:de:43' ip='192.168.122.31'/>
            <host mac='52:54:00:61:c4:85' ip='192.168.122.100'/>
        </dhcp>
</network>
```

Une fois cela fait, on éteint toutes nos VM, on détruit et re-créer notre network, et à leur prochain démarrage les VM récupéreront la bonne adresse IP
```sh
# Dans virsh
net-destroy default
net-create default
```

# Configuration des VMs

## Installation de K3S

- Afin d'installer k3s sur votre premier node Master veuillez réaliser les commandes ci-dessous. Celle-ci nous permet d'initier un cluster pour nos futurs machines :
```
curl -fL https://get.k3s.io | sh -s - server --cluster-init --disable-apiserver --disable-controller-manager --disable-scheduler
```

- Ensuite optener sur votre server le TOKEN de K3S:
```
 sudo cat /var/lib/rancher/k3s/server/node-token
```

- Puis sur les autres machines master réaliser la commande suivante en remplacant notre ip par la votre afin que celles-ci rejoignent le cluster, vous pouvez aussi choisir d'autoriser l'ETCD* en fonction de vos besoins  :
 
\* ETCD joue un rôle essentiel dans K3s en fournissant un stockage cohérent, une coordination distribuée et la découverte de services pour le cluster Kubernetes. Il contribue à garantir la stabilité, la cohérence et la résilience du cluster.

```
curl -fL https://get.k3s.io | sh -s - server --token <token> --disable-etcd --server https://192.168.122.10:6443 
```

- Tester avec la commande que votre machine sois bien rentrée dans votre cluster:
```
$ kubectl get nodes
NAME                       STATUS   ROLES                       AGE     VERSION
m1.projet.infra            Ready    etcd                        0h31m   v1.20.4+k3s1
m2.projet.infra            Ready    control-plane,master        0h28m   v1.20.4+k3s1
```

- Pour les machines worker, réaliser la commande suivante:
```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="agent --server https://192.168.122.10 --token <token>" sudo sh -s -
```

## Déploiement d'un wordpress à la main

- Créer un dossier wordpress:
```
mkdir wordpress
```

- Un Secret est un objet qui stocke une donnée sensible comme un mot de passe ou une clé. Kubectl supporte la gestion des objets Kubernetes à l'aide d'un fichier de personnalisation. Vous pouvez créer un Secret par des générateurs dans kustomization.yaml.

```
cat <<EOF >./kustomization.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
EOF
```

- Télécharger le fichier de déploiement Mysql pour kubernetes:
```
curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
```
[mysql-deployment.yaml](config/wordpress/mysql-deployment.yaml)

- Télécharger le fichier de configuration de wordpress:
```
curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```
[wordpress-deployment.yaml](config/wordpress/wordpress-deployment.yaml)
- Ajouter les dans votre fichier kustomization.yaml.
```
cat <<EOF >>./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF
```
[kustomization.yaml](config/wordpress/kustomization.yaml)
- Appliquer la configuration

```
kubectl apply -k wordpress/
```

- vérifier que le pod run avec la commande `kubectl get pods`

- Observer qu'elle IP est associer a votre service wordpress avec la commande suivante: `kubectl get pods -o wide`

- Puis aller dans votre navigateur et verifier le fonctionnement de wordpress avec son ip: 

![wordpress](./pics/wordpress.png)


## Déploiement de la stack Prometheus via Helm

- Ajout du repository prometheus a helm : 
``` 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

- Dans notre cas nous voulions utilisé le monitoring de graphana:
```
helm install monitoring prometheus-community/prometheus
```

- On vérifie le fonctionnement avec la même méthode que pour wordpress:

![grafana](./pics/grafana.png)

# Registre d'images signées

Pour garantir l'authenticité des images que nous utilisons nous voulions mettre en place un registre d'images signées.
Nous avons donc crée une VM `registry1`.

## Trivy

Avant d'envoyer nos images sur notre registre nous voulons nous assurer que ces dernières ne comportent pas de vulnérabilitées critiques.
Nous avons donc installé trivy qui est un outil open source qui permet de scanner des images docker et d'afficher les vulnérabilitées que cette image comporte. Il se base sur des vulnérabilités connues (CVEs) pour établir ses rapports.

Installation de trivy :
```shell
dnf install trivy 
```

Utilisation de trivy :
![trivy](./pics/scan.png)

## Notary

Notary est un outil open source qui permet de chiffrer et de signer des images docker.
A cause de multiples erreurs nous n'avons pas pu finir cette partie du projet donc nous n'avons pas notre registre d'images signées.

Notre but était de scanner l'image avec trivy et ensuite, si elle ne comporte pas de vulnérabilitées critiques de la push vers notre regitre qui signe l'image et la stocke.

# CIS Benchmark

Nous avons ensuite fait du durcissement sur le serveur physique en suivant le CIS Benchmark sur Debian 11.
Le CIS (Center for Internet Security) est une organisation à but non lucratif créée en octobre 2000. Sa mission consiste à aider les personnes, les entreprises et les gouvernements à se protéger contre les cybermenaces.

Pour cela ils publient notamment des guides de durcissment dont celui sur Debian 11 que nous avons utilisé.

Nous n'avons pas appliqué toutes les recommandations listées sur le benchmark, nous avons appliqué celles qui nous semblait les plus pertinantes dans le cadre de notre projet.

## Partitionnement

Il est recommandé de créer plusieurs partitions sur le disque, le CIS recommande de séparer les partitions `/tmp`, `/var`, `/var/tmp`, `/var/log`, `/var/log/audit` et `/home`.
Pour vérifier si le partitionnement est correct on peut faire la commande suivante pour chaque partition :

```shell
findmnt --kernel /tmp
/tmp tmpfs tmpfs rw,nosuid,nodev,noexec,inode6

findmnt --kernel /home
/home /dev/sdb ext4 rw,nosuid,nodev,relatime,seclabel
```

## SSH

Comme expliqué [précédemment](#accès-ssh) nous avons effectué des modifications dans le fichier de configuration de SSH `/etc/ssh/sshd_config` pour modifier le port utilisé, autoriser les connexions par clé SSH et refuser les connexions avec un mot de passe.

Pour durcir encore plus notre accès SSH nous avons limité l'accès. Il y a plusieurs options disponnibles pour limiter quels utilisateurs ou groupes peuvent accéder au système via SSH. Nous avons utilisé l'option `AllowUsers` et nous avons ajouté les utilisateurs nécessaires.
```shell
AllowUsers benjamin, hugo, thomas
```
Nous avons désactivé la connexion root car cela limite les possibilités de non-répudiation et fournit une piste d'audit claire en cas d'incident de sécurité.
```shell
PermitRootLogin no
```
Nous avons désactivé le X11 forwarding car nous n'utilisons pas de connexion graphique à distance.
```shell
X11Forwarding no
```
